Enhancing Protein Contact Map Prediction Accuracy via Ensembles of Inter-Residue Distance Predictors
This project provides the programs and related files to construct contact map via ensembles of Inter-Residue Distance Predictors.


Requirements
To run the program, for each protein named proteinID, you need the following files

trRosettaX output file (https://yanglab.nankai.edu.cn/trRosetta/)
RealDist Output file (https://github.com/ba-lab/realdist)
DeepDist Output file (https://github.com/multicom-toolbox/deepdist)

All of these files you can generate by using their Git-hub repository as well as online server(for trRosettaX). All input files must be in a folder named by input directory and their corresponding predictors name.


Running the Program
	Update the protein_list.lst file with the names of the proteins need to predict
	Put the all needed input files inside the corresponding predictors name folders of input folder
	Run bash predict.sh from the root folder 
	The outputs are generated in the Output folder
