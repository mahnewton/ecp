#%% Env check 
import sys
if sys.version_info < (3,0,0):
    print('Python 3 required!!!')
    sys.exit(1)

#%% parse input-output options
import os
import getopt
import numpy as np

print(sys.argv[1:])

def show_usage_info():
    print('Usage:' + sys.argv[0] + ' -d DeepDist_directory -r RealDist_directory -t trRosettaX_directory -o output_directory -p Protein_ID')

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:r:t:o:p:h")
except getopt.GetoptError as err:
    print(err)
    show_usage_info()
    sys.exit(2)

for opt, opt_val in opts:
    if opt in ("-h", "--help"):
        show_usage_info()
        sys.exit()
    elif opt in ("-d"):
        DEEPDIST_DIST_DIR = os.path.abspath(opt_val)  
    
    elif opt in ("-r"):
        REALDIST_DIST_DIR = os.path.abspath(opt_val)
       
    elif opt in ("-t"):
        trRosettaX_DIST_DIR = os.path.abspath(opt_val)
    
    elif opt in ("-o"):
        OUTPUT_DIR = os.path.abspath(opt_val) 
    elif opt in ("-p"):
        proteinID = opt_val
    else:
        print('Unhandled option provided.')
        show_usage_info()
        sys.exit(1)

def is_all_data_exists(DEEPDIST_DIST_DIR, REALDIST_DIST_DIR, trRosettaX_DIST_DIR, proteinID):
    trRosettaX_DIST_File = os.path.join(trRosettaX_DIST_DIR, proteinID) + '.npz'
    REALDIST_DIST_File = os.path.join(REALDIST_DIST_DIR, proteinID) + '.realdist.npy'
    DEEPDIST_DIST_File = os.path.join(DEEPDIST_DIST_DIR, proteinID) + '.txt'


    
    if os.path.exists(trRosettaX_DIST_File) and os.path.exists(REALDIST_DIST_File) and os.path.exists(DEEPDIST_DIST_File):
        return True
    else:
        
        if not os.path.exists(trRosettaX_DIST_File): print(trRosettaX_DIST_File)
        if not os.path.exists(REALDIST_DIST_File): print(REALDIST_DIST_File)
        if not os.path.exists(DEEPDIST_DIST_File): print(DEEPDIST_DIST_File)
        print("One or more data file missing. Check all files present in the input data directory.")
        return False


if not is_all_data_exists(DEEPDIST_DIST_DIR, REALDIST_DIST_DIR, trRosettaX_DIST_DIR, proteinID):
    exit(1)
if not os.path.exists(OUTPUT_DIR):
    print('Output path not exists at: ' + OUTPUT_DIR)
    exit(1)


def distance_to_contacts(y_pred):
    P = np.full((y_pred.shape[0], y_pred.shape[0]), 0.0)
    y_pred = np.nan_to_num(y_pred)
    for i in range(y_pred.shape[0]):
        for j in range(y_pred.shape[1]):
            if y_pred[i][j] !=0.0:
                P[i][j] = 4.0/y_pred[i][j]
                
    P[P > 1.0] = 1.0
    np.fill_diagonal(P, 1.0)     
    return P

#proteinID = DEEPDIST_DIST_DIR.split('/')[-1]
## binned distance predictor
trRosettaX_file = np.load(os.path.join(trRosettaX_DIST_DIR, proteinID)+'.npz')
trRosettaX_dist = trRosettaX_file['dist.npy']

## real-valued distance predictor
realdist_dist = np.load(os.path.join(REALDIST_DIST_DIR, proteinID)+'.realdist.npy')
deepdist_dist = np.loadtxt(os.path.join(DEEPDIST_DIST_DIR, proteinID)+'.txt')

## distance to contact probability
L = trRosettaX_dist.shape[0]
trRosettaX_contact = np.zeros((L, L))
for j in range(0, L):
    for k in range(j, L):
        trRosettaX_contact[j,k] = np.sum(trRosettaX_dist[j,k,1:13])
        
realdist_contact = distance_to_contacts(np.triu(realdist_dist,k=0))
deepdist_contact = distance_to_contacts(np.triu(deepdist_dist,k=0))


contact_ensemble = np.mean((realdist_contact, deepdist_contact, trRosettaX_contact), axis=0)

#save contact probability matrix
np.save(os.path.join(OUTPUT_DIR, proteinID + '.npy'), contact_ensemble)
